# TATA: Taint Analysis for Transient Analysis

TATA (Taint Analysis for Transient Analysis) is a tool that automatically detects, at compilation time, missing transient keywords directly from Android applications’ source code composed of Dalvik bytecode and C/C++.

The tool is composed of two parts:

1. A version modified of [clang](https://clang.llvm.org/) which contains a new static analyzer. This static analyzer is in charge of finding the set of C/C++ variables that contain pointer to memory and are sent to the bytecode. This set is stored in a JSON file.

2. A Java 8 project that uses [FlowDroid](https://github.com/secure-software-engineering/FlowDroid) to track pointer values inside bytecode or Java source code. Any problematic field is reported directly on stdout.

## Usage

This repository contains the pre-compiled binaries of TATA:
- [tata_clang](tata_clang) : the modified version of clang.
- [merge_multiple_json.py](merge_multiple_json.py) : a script that merges several json files into a unique one.
- [tata_java.jar](tata_java.jar) : the jar of the Java 8 project.

The analysis is done in three steps:

1. Launch C/C++ analysis

First, each C/C++ file that composes the native part of the application have to be processed by the custom clang static analyzer. This is down by invoking `tata_clang` using specific arguments to request the static analysis. When running, the static analyzer outputs a json file on `stderr`.

```
tata_clang -cc1 -analyze -analyzer-checker=alpha.unix.NativePointerField ORIGINAL_COMPILATION_COMMAND_LINE_ARGS 2>json_file
```

2. Merge json files

Then, all the json files (one for each C/C++ file) have to be merged into a unique one using the given python script:

```
python3 merge_multiple_json.py file1 file2 [...]
```

3. Launch Java source or Dalvik Bytecode analysis

Finally, the whole Java application source code should be processed by TATA using tata_jar. This command receives as argument:
- A path to the directory of the application Java sources (.java) or the application Java classes (.class) (`src_or_class_dir`). Both are treated transparently.
- A unique json file generated during the previous step (`json_file`).
- A path to an apk file containing the manifest file of the application (`-m apk_with_manifest_file`) or the name of a class of the application (`-c main_classname`). When a manifest is provided, it is parsed to retrieve all the possible entry-points of the application. All these entry-points are then used as starting points of the analysis. When the name of a class is provided, only the default constructor of this class is used as a starting point of the analysis.
- A path to the Java runtime environment library (`optional_java_lib_path`). This parameter is optional. The default value is `/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar`.
- An optional switch (`--analyze-non-serializable`) which forces the analysis to be conducted on non serializable classes.

```
java -jar tata_java.jar src_or_class_dir json_file -m apk_with_manifest_file [optional_java_lib_path] [--analyze-non-serializable]
java -jar tata_java.jar src_or_class_dir json_file -c main_classname [optional_java_lib_path] [--analyze-non-serializable]
```

The result of the analysis is directly written to `stdout`.

:warning: **The analysis should be run using Java 8**

## Recompiling

TATA source code is composed of two repositories:
- [TATA-java](https://gitlab.inria.fr/cidre-public/tata/tata-java): contains the bytecode analyzer based on Flowdroid. This is an eclipse project that can be imported and directly compiled.
- [TATA-llvm](https://gitlab.inria.fr/cidre-public/tata/tata-llvm): contains the C/C++ analyzer based on clang. The compilation process is described in the project [README.md](https://gitlab.inria.fr/cidre-public/tata/tata-llvm/-/blob/master/README.md) file. The TATA analyzer is located in the file [clang/lib/StaticAnalyzer/Checkers/NativePointerFieldChecker.cpp](https://gitlab.inria.fr/cidre-public/tata/tata-llvm/-/blob/master/clang/lib/StaticAnalyzer/Checkers/NativePointerFieldChecker.cpp) inside the TATA-llvm repository.
