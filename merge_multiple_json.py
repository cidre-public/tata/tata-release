#/usr/bin/env python3
import sys
import json

if len(sys.argv) < 2:
    print("usage: %s file1 file2 [...]" % sys.argv[0])
    sys.exit(1)

pointerReturnValues = set()
pointerArgs = set()
pointerFields = set()
def update_values_with_new_json(j):
    pointerReturnValues.update(j["pointerReturnValues"])
    for d in j["pointerArgs"]:
        pointerArgs.add((d['meth'],d['pos']))
    pointerFields.update(j["pointerFields"])
def output_new_json():
    pointerArgsDict = list()
    for (a,b) in pointerArgs:
        pointerArgsDict.append({'meth':a, 'pos':b})
    return json.dumps({'pointerReturnValues': sorted(list(pointerReturnValues)),
                       'pointerArgs':pointerArgsDict,
                       'pointerFields':sorted(list(pointerFields))},
                      indent=4)

json_content = ""
next_file = ""
for argv in sys.argv[1:]:
    with open(argv) as f:
        in_json = False
        for l in f:            
            if l == "{\n":
                in_json = True
            if in_json:
                json_content += l
            if l == "}\n":
                in_json = False
                j = json.loads(json_content)
                json_content = ""
                update_values_with_new_json(j)
print(output_new_json())
